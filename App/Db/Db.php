<?php

namespace App\Db;

use \PDO;
use \PDOException;

class Db{

	const HOST_NAME = '127.0.0.1';
  	const DB_NAME = 'kabum';
  	const DB_USER = 'root';
  	const DB_PASSWORD = '';

	private $connection;

	public static function table(){
		return NULL;
	}

	public function __construct(){
		$this->setConnection();
	}

	private function setConnection(){
    	try{
      		$this->connection = new PDO('mysql:host='.self::HOST_NAME.';dbname='.self::DB_NAME,self::DB_USER,self::DB_PASSWORD);
      		$this->connection->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
		}catch(PDOException $e){
      		die('ERROR: '.$e->getMessage());
    	}
  	}

  	public function execute($query,$params = []){
    	try{
      		$statement = $this->connection->prepare($query);
      		$statement->execute($params);
      		return $statement;
    	}catch(PDOException $e){
      		die('ERROR: '.$e->getMessage());
    	}
  	}

	public function insert($values){
    
    	$fields = array_keys($values);
    	$binds  = array_pad([],count($fields),'?');

    	$query = 'INSERT INTO '.static::table().' ('.implode(',',$fields).') VALUES ('.implode(',',$binds).')';

    	$this->execute($query,array_values($values));

    	return $this->connection->lastInsertId();
  	}

	public function select($where = null, $order = null, $limit = null, $fields = '*'){
		
		$where = strlen($where) ? 'WHERE '.$where : '';
		$order = strlen($order) ? 'ORDER BY '.$order : '';
		$limit = strlen($limit) ? 'LIMIT '.$limit : '';

		$query = 'SELECT '.$fields.' FROM '.static::table().' '.$where.' '.$order.' '.$limit;

		return $this->execute($query);
	}

	public function update($where,$values){
		$fields = array_keys($values);

		$query = 'UPDATE '.static::table().' SET '.implode('=?,',$fields).'=? WHERE '.$where;

		$this->execute($query,array_values($values));

		return true;
	}

	public function delete($where){
		$query = 'DELETE FROM '.static::table().' WHERE '.$where;

		$this->execute($query);

		return true;
	}

	public function getAll($where = null, $order = null, $limit = null){
		return $this->select($where,$order,$limit)->fetchAll(PDO::FETCH_CLASS,get_called_class());
	}

	public function getOne($id){
		return $this->select('id = '.$id)->fetchObject(get_called_class());
	}

}