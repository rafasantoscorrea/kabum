<?php

namespace App\Model;

use \App\Db\Db;

class Usuario extends Db{

	public $id;
	public $login;
	public $nome;
	public $senha;

	public static function table(){
		return 'usuario';
	}

	public function cadastrar(){
	    $this->id = $this->insert([
			'login' => $this->login,
			'nome' => $this->nome,
			'senha' => $this->senha
        ]);
	    return true;
  	}

	public function atualizar(){
		return $this->update('id = '.$this->id,[
            'login' => $this->login,
			'nome' => $this->nome,
			'senha' => $this->senha
      	]);
	}

	public function excluir(){
		return $this->delete('id = '.$this->id);
	}
}


?>