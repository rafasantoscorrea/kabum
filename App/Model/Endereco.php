<?php

namespace App\Model;

use \App\Db\Db;

class Endereco extends Db{

	public $id;
	public $logradouro;
	public $numero;
	public $complemento;
	public $bairro;
	public $cidade;
	public $uf;
	public $cep;
	public $cliente_id;

	public $retorno = [
		'success' => true,
		'message' => []
	];

	public static function table(){
		return 'endereco';
	}

	public function cadastrar(){
		$this->retorno['acao'] = 'Cadastrar';
		self::convertDb();
		self::validate();

		if($this->retorno['success']){
		    $this->id = $this->insert([
				'logradouro' => $this->logradouro,
				'numero' => $this->numero,
				'complemento' => $this->complemento,
				'bairro' => $this->bairro,
				'cidade' => $this->cidade,
				'uf' => $this->uf,
				'cep' => $this->cep,
				'cliente_id' => $this->cliente_id
	        ]);
		}

	    return $this->retorno;
  	}

	public function atualizar(){
		$this->retorno['acao'] = 'Atualizar';
		self::convertDb();
		self::validate();

		if($this->retorno['success']){
			$this->update('id = '.$this->id,[
	            'logradouro' => $this->logradouro,
				'numero' => $this->numero,
				'complemento' => $this->complemento,
				'bairro' => $this->bairro,
				'cidade' => $this->cidade,
				'uf' => $this->uf,
				'cep' => $this->cep,
	      	]);
		}

      	return $this->retorno;
	}

	public function excluir(){
		return $this->delete('id = '.$this->id);
	}

	private function validate(){
		self::validaCep();
		self::validaLogradouro();
		self::validaNumero();
		self::validaBairro();
		self::validaCidade();
		self::validaUf();

		if(!empty($this->retorno['message'])){
			$this->retorno['success'] = false;
		}
	}

	private function convertDb(){
		$this->cep = preg_replace('/[^0-9]/', "",$this->cep);
	}

	private function validaCep(){
		if(empty($this->cep)){
			$this->retorno['message'][] = 'CEP Obrigatório';
		}elseif(strlen($this->cep)<7){
			$this->retorno['message'][] = 'CEP Incompleto';
		}
	}

	private function validaLogradouro(){
		if(empty($this->logradouro)){
			$this->retorno['message'][] = 'Logradouro Obrigatório';
		}
	}

	private function validaNumero(){
		if(empty($this->numero)){
			$this->retorno['message'][] = 'Número Obrigatório';
		}
	}

	private function validaBairro(){
		if(empty($this->bairro)){
			$this->retorno['message'][] = 'Bairro Obrigatório';
		}
	}

	private function validaCidade(){
		if(empty($this->cidade)){
			$this->retorno['message'][] = 'Cidade Obrigatório';
		}
	}

	private function validaUf(){
		if(empty($this->uf)){
			$this->retorno['message'][] = 'UF Obrigatório';
		}
	}
}


?>