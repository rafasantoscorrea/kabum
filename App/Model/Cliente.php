<?php

namespace App\Model;

use \App\Db\Db;
use \App\Utils\ValidaCpf;

class Cliente extends Db{

	public $id;
	public $nome;
	public $data_nascimento;
	public $cpf;
	public $rg;
	public $telefone;

	public $retorno = [
		'success' => true,
		'message' => []
	];

	public static function table(){
		return 'cliente';
	}

	public function cadastrar(){
		$this->retorno['acao'] = 'Cadastrar';
		self::convertDb();
		self::validate();

		if($this->retorno['success']){
		    $this->id = $this->insert([
				'nome' => $this->nome,
				'data_nascimento' => $this->data_nascimento,
				'cpf' => $this->cpf,
				'rg' => $this->rg,
				'telefone' => $this->telefone,
	        ]);
		}

	    return $this->retorno;
  	}

	public function atualizar(){
		$this->retorno['acao'] = 'Atualizar';
		self::convertDb();
		self::validate();

		if($this->retorno['success']){
			$this->update('id = '.$this->id,[
	            'nome' => $this->nome,
				'data_nascimento' => $this->data_nascimento,
				'cpf' => $this->cpf,
				'rg' => $this->rg,
				'telefone' => $this->telefone,
	      	]);
		}

      	return $this->retorno;
	}

	public function excluir(){
		self::deleteRelated();
		return $this->delete('id = '.$this->id);
	}

	private function deleteRelated(){
		$modelEndereco = (new Endereco)->getAll('cliente_id = '.$this->id);
		foreach($modelEndereco as $endereco){
			$endereco->excluir();
		}
	}

	private function validate(){
		self::validaCpf();
		self::validaNome();
		self::validaDataNascimento();
		self::validaRg();
		self::validaTelefone();

		if(!empty($this->retorno['message'])){
			$this->retorno['success'] = false;
		}
	}

	private function convertDb(){
		$this->cpf = preg_replace('/[^0-9]/', "",$this->cpf);
		$this->telefone = preg_replace('/[^0-9]/', "",$this->telefone);
	}

	private function validaCpf(){
		if(empty($this->cpf)){
			$this->retorno['message'][] = 'CPF Obrigatório';
		}elseif(!(new ValidaCpf)->check($this->cpf)){
			$this->retorno['message'][] = 'CPF inválido';
		}
	}

	private function validaNome(){
		if(empty($this->nome)){
			$this->retorno['message'][] = 'Nome Obrigatório';
		}
	}

	private function validaDataNascimento(){
		if(empty($this->data_nascimento)){
			$this->retorno['message'][] = 'Data de Nascimento Obrigatório';
		}
	}

	private function validaRg(){

	}

	private function validaTelefone(){
		if(strlen($this->telefone)<10 && !empty($this->telefone)){
			$this->retorno['message'][] = 'Telefone incompleto';
		}	
	}
}


?>