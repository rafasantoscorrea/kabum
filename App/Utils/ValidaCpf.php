<?php

namespace App\Utils;

class ValidaCpf{

	public function check($cpf){
        if(!$cpf){
            return false;
        }
        
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );

        if (strlen( $cpf ) != 11){
            return false;
        }
        
        $digitos = substr($cpf, 0, 9);
        
        $novo_cpf = $this->calc_digitos($digitos);

        $novo_cpf = $this->calc_digitos( $novo_cpf, 11 );

        if ( $novo_cpf === $cpf ) {
            return true;
        } else {
            return false;
        }
    }
    
    public function calc_digitos($digitos, $posicoes = 10, $soma_digitos = 0 ) {
        for($i = 0; $i < strlen( $digitos ); $i++){
            $soma_digitos = $soma_digitos + ( $digitos[$i] * $posicoes );
            $posicoes--;
        }
        $soma_digitos = $soma_digitos % 11;

        if( $soma_digitos < 2 ){
            $soma_digitos = 0;
        }else{
            $soma_digitos = 11 - $soma_digitos;
        }
        $cpf = $digitos . $soma_digitos;

        return $cpf;
    }
}