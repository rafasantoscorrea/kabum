<?php

namespace App\Controller;

use \App\Utils\View;
use \App\Model\Cliente;
use \App\Controller\EnderecoController;

class ClienteController extends Controller{


	public static function list(){

		$model = (new Cliente)->getAll();
		$itens = '';

		foreach($model as $cliente){
			$itens.= View::render('cliente/_item', [
				'id' => $cliente->id,
				'nome' => $cliente->nome,
				'data_nascimento' => date_format(date_create($cliente->data_nascimento),"d/m/Y"),
				'cpf' => $cliente->cpf,
				'rg' => $cliente->rg,
				'telefone' => $cliente->telefone,
			]);
		}
		$content = View::render('cliente/list', [
			'itens' => $itens
		]);

		return parent::getView('Clientes - Listagem', $content);
	}

	public static function create($model = [], $error = ''){

		$cliente = empty($model) ? (new Cliente) : $model;

		$content = View::render('cliente/create', [
			'error' => $error,
			'nome' => $cliente->nome,
			'data_nascimento' => $cliente->data_nascimento,
			'cpf' => $cliente->cpf,
			'rg' => $cliente->rg,
			'telefone' => $cliente->telefone,
		]);

		return parent::getView('Clientes - Criação', $content);
	}

	public static function createPost($request){
		$post = $request->getPostVars();

		$model = new Cliente;
		$model->nome = $post['Cliente']['nome'];
		$model->data_nascimento = $post['Cliente']['data_nascimento'];
		$model->cpf = $post['Cliente']['cpf'];
		$model->rg = $post['Cliente']['rg'];
		$model->telefone = $post['Cliente']['telefone'];

		$retorno = $model->cadastrar();

		if($retorno['success']){
			return ['id' => $model->id];
		}else{
			return self::create($model, parent::getError($retorno));
		}
	}

	public static function view($id){
		$cliente = (new Cliente)->getOne($id);

		$content = View::render('cliente/view', [
			'id' => $cliente->id,
			'nome' => $cliente->nome,
			'data_nascimento' => $cliente->data_nascimento,
			'cpf' => $cliente->cpf,
			'rg' => $cliente->rg,
			'telefone' => $cliente->telefone,
			'enderecos' => EnderecoController::list($id)
		]);

		return parent::getView('Cliente - Visualização', $content);
	}

	public static function update($id, $model = [], $error = ''){
		$cliente = empty($model) ? (new Cliente)->getOne($id) : $model;

		$content = View::render('cliente/update', [
			'error' => $error,
			'id' => $cliente->id,
			'nome' => $cliente->nome,
			'data_nascimento' => $cliente->data_nascimento,
			'cpf' => $cliente->cpf,
			'rg' => $cliente->rg,
			'telefone' => $cliente->telefone,
		]);

		return parent::getView('Cliente - Atualização', $content);
	}

	public static function updatePost($id, $request){
		$model = (new Cliente)->getOne($id);

		$post = $request->getPostVars();

		$model->nome = $post['Cliente']['nome'];
		$model->data_nascimento = $post['Cliente']['data_nascimento'];
		$model->cpf = $post['Cliente']['cpf'];
		$model->rg = $post['Cliente']['rg'];
		$model->telefone = $post['Cliente']['telefone'];

		$retorno = $model->atualizar();

		if($retorno['success']){
			return ['id' => $model->id];
		}else{
			return self::update($model->id, $model, parent::getError($retorno));
		}
	}

	public static function delete($id){
		$model = (new Cliente)->getOne($id);

		$model->excluir();

		return true;
	}
}