<?php

namespace App\Controller;

use \App\Utils\View;
use \App\Model\Endereco;

class EnderecoController extends Controller{

	public static function list($cliente_id){

		$model = (new Endereco)->getAll('cliente_id = '.$cliente_id);
		$itens = '';

		foreach($model as $endereco){
			$itens.= View::render('endereco/_item', [
				'id' => $endereco->id,
				'cep' => $endereco->cep,
				'logradouro' => $endereco->logradouro,
				'numero' => $endereco->numero,
				'complemento' => $endereco->complemento,
				'bairro' => $endereco->bairro,
				'cidade' => $endereco->cidade,
				'uf' => $endereco->uf
			]);
		}
		return View::render('endereco/list', [
			'itens' => $itens,
			'cliente_id' => $cliente_id
		]);
	}

	public static function create($cliente_id, $model = [], $error = ''){
		$endereco = empty($model) ? (new Endereco) : $model;

		$content = View::render('endereco/_form', [
			'error' => $error,
			'cep' => $endereco->cep,
			'logradouro' => $endereco->logradouro,
			'numero' => $endereco->numero,
			'complemento' => $endereco->complemento,
			'bairro' => $endereco->bairro,
			'cidade' => $endereco->cidade,
			'uf' => $endereco->uf,
			'cliente_id' => $cliente_id
		]);

		return parent::getView('Endereço - Criação', $content);
	}

	public static function createPost($cliente_id, $request){
		$post = $request->getPostVars();

		$model = new Endereco;
		$model->cep = $post['Endereco']['cep'];
		$model->logradouro = $post['Endereco']['logradouro'];
		$model->numero = $post['Endereco']['numero'];
		$model->complemento = $post['Endereco']['complemento'];
		$model->bairro = $post['Endereco']['bairro'];
		$model->cidade = $post['Endereco']['cidade'];
		$model->uf = $post['Endereco']['uf'];
		$model->cliente_id = $cliente_id;

		$retorno = $model->cadastrar();


		if($retorno['success']){
			return ['cliente_id' => $model->cliente_id];
		}else{
			return self::create($cliente_id, $model, parent::getError($retorno));
		}
	}

	public static function update($id, $model = [], $error = ''){
		$endereco = empty($model) ? (new Endereco)->getOne($id) : $model;

		$content = View::render('endereco/_form', [
			'error' => $error,
			'id' => $endereco->id,
			'cep' => $endereco->cep,
			'logradouro' => $endereco->logradouro,
			'numero' => $endereco->numero,
			'complemento' => $endereco->complemento,
			'bairro' => $endereco->bairro,
			'cidade' => $endereco->cidade,
			'uf' => $endereco->uf,
			'cliente_id' => $endereco->cliente_id
		]);

		return parent::getView('Endereço - Atualização', $content);
	}

	public static function updatePost($id, $request){
		$model = (new Endereco)->getOne($id);

		$post = $request->getPostVars();

		$model->cep = $post['Endereco']['cep'];
		$model->logradouro = $post['Endereco']['logradouro'];
		$model->numero = $post['Endereco']['numero'];
		$model->complemento = $post['Endereco']['complemento'];
		$model->bairro = $post['Endereco']['bairro'];
		$model->cidade = $post['Endereco']['cidade'];
		$model->uf = $post['Endereco']['uf'];

		$retorno = $model->atualizar();

		if($retorno['success']){
			return ['cliente_id' => $model->cliente_id];
		}else{
			return self::update($model->id, $model, parent::getError($retorno));
		}
	}

	public static function delete($id){
		$model = (new Endereco)->getOne($id);

		$model->excluir();

		return ['cliente_id' => $model->cliente_id];
	}
}