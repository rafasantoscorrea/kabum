<?php

require 'vendor/autoload.php';

use \App\Utils\View;
use \App\Http\Router;
use \App\Http\Response;
use \App\Controller\DefaultController;
use \App\Controller\ClienteController;
use \App\Controller\EnderecoController;
	
define('URL', 'http://localhost/kabum');


View::init([
	'URL' => URL
]);

$router = new Router(URL);



$router->get('/',[
	function(){
		return new Response(200, DefaultController::home());
	}
]);

/*-----------------------------------Cliente-------------------------------*/
$router->get('/cliente/create',[
	function(){
		return new Response(200, ClienteController::create());
	}
]);

$router->post('/cliente/create',[
	function($request){
		$response = ClienteController::createPost($request);
		if(isset($response['id'])){
			header('Location: '.URL.'/cliente/view/'.$response['id']);
		}else{
			return new Response(200, $response);
		}
	}
]);

$router->get('/cliente/list',[
	function(){
		return new Response(200, ClienteController::list());
	}
]);

$router->get('/cliente',[
	function(){
		return new Response(200, ClienteController::list());
	}
]);

$router->get('/cliente/view/{id}',[
	function($id){
		return new Response(200, ClienteController::view($id));
	}
]);

$router->get('/cliente/update/{id}',[
	function($id){
		return new Response(200, ClienteController::update($id));
	}
]);

$router->post('/cliente/update/{id}',[
	function($id, $request){
		$response = ClienteController::updatePost($id, $request);
		if(isset($response['id'])){
			header('Location: '.URL.'/cliente/view/'.$response['id']);
		}else{
			return new Response(200, $response);
		}
	}
]);

$router->get('/cliente/delete/{id}',[
	function($id){
		$response = ClienteController::delete($id);
		if($response === true){
			header('Location: '.URL.'/cliente/list');
		}else{
			return new Response(200, $response);
		}
	}
]);

/*-----------------------------------Endereço-------------------------------*/
$router->get('/endereco/create/{cliente_id}',[
	function($cliente_id){
		return new Response(200, EnderecoController::create($cliente_id));
	}
]);

$router->post('/endereco/create/{cliente_id}',[
	function($cliente_id, $request){
		$response = EnderecoController::createPost($cliente_id, $request);
		if(isset($response['cliente_id'])){
			header('Location: '.URL.'/cliente/view/'.$response['cliente_id']);
		}else{
			return new Response(200, $response);
		}
	}
]);

$router->get('/endereco/update/{id}',[
	function($id){
		return new Response(200, EnderecoController::update($id));
	}
]);

$router->post('/endereco/update/{id}',[
	function($id, $request){
		$response = EnderecoController::updatePost($id, $request);
		if(isset($response['cliente_id'])){
			header('Location: '.URL.'/cliente/view/'.$response['cliente_id']);
		}else{
			return new Response(200, $response);
		}
	}
]);

$router->get('/endereco/delete/{id}',[
	function($id){
		$response = EnderecoController::delete($id);
		if(isset($response['cliente_id'])){
			header('Location: '.URL.'/cliente/view/'.$response['cliente_id']);
		}else{
			return new Response(200, $response);
		}
	}
]);

$router->run()->sendResponse();
	

?>