<?php

namespace App\Controller;

use \App\Utils\View;

class Controller{

	private static function getHeader(){
		return View::render('layout/header');
	}

	private static function getFooter(){
		return View::render('layout/footer');
	}

	public static function getView($title, $content){
		return View::render('layout/main', [
			'header' => self::getHeader(),
			'title' => $title,
			'content' => $content, 
			'footer' => self::getFooter(),
		]);
	}

	public static function getError($retorno){
		return View::render('layout/error', [
			'errorList' => self::getErrorList($retorno['message']),
			'acao' => $retorno['acao']
		]);	
	}

	private static function getErrorList($errors){
		$errorList = '';

		foreach($errors as $error){
			$errorList.= View::render('layout/_errorList', ['line' => $error]);
		}

		return $errorList;
	}
}